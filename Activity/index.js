/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

		//first function here:
		function fullnameAgeLocation() {
			let fullName = prompt("Enter Your Full Name");
			let Age = prompt("Enter Your Age");
			let Location = prompt("Enter Your Location");

			console.log("Hello, " + fullName);
			console.log("You are " + Age + " years old.");
			console.log("You live in " + Location)
		}
		
		fullnameAgeLocation();
		
	

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.	
*/

		//second function here:
		function myBands() {
			let first = "Eraser Heads";
			let second = "Parokya ni Edgar";
			let third = "Callalily";
			let fourth = "Sandwich";
			let fifth = "Join the Club";

			console.log ("1. " + first);
			console.log ("2. " + second);
			console.log ("3. " + third);
			console.log ("4. " + fourth);
			console.log ("5. " + fifth);
		}

		myBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
		//third function here:
		function myMovies() {
			let isa = "Wolf of Wallstreet";
			let isarating = "Rotten Tomatoes Rating: 97%";
			let dalawa = "Project Almanac";
			let dalawarating = "Rotten Tomatoes Rating: 98%";
			let tatlo = "Snowden";
			let tatlorating = "Rotten Tomatoes Rating: 99%";
			let apat = "Project X";
			let apatrating = "Rotten Tomatoes Rating: 100%";
			let lima = "The Notebook";
			let limarating = "Rotten Tomatoes Rating: 99%";

			console.log ("1. " + isa);
			console.log (isarating);
			console.log ("2. " + dalawa);
			console.log (dalawarating);
			console.log ("3. " + tatlo);
			console.log (tatlorating);
			console.log ("4. " + apat);
			console.log (apatrating);
			console.log ("5. " + lima);
			console.log (limarating);
		}

		myMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

		
		let printFriends = function printUsers() {
			alert("Hi! Please add the names of your friends.");
			let friend1 = prompt("Enter your first friend's name:"); 
			let friend2 = prompt("Enter your second friend's name:"); 
			let friend3 = prompt("Enter your third friend's name:");

			console.log("You are friends with:")
			console.log(friend1); 
			console.log(friend2); 
			console.log(friend3); 
		};

		printFriends();
		

		
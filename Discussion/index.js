console.log("Hello World")
/*
	The statements and instructions inside a function is not immediately executed when the function is defined.
	They are run/executed when a function is invoked.
	To invoke a declared function, add the name of the function and a parenthesis.

*/

// Hoisting - javascript behavior for certain variables and functions to run to use them before their declaration

	printName();

	function printName() {
		console.log("My Name is John")
		console.log("My Name is John")
		console.log("My Name is John")
	}

	printName();

	let variableFunction = function myGreetings() {
		console.log("Hello");
	}

	// variableFunction()

// Re-Assigning functions

	variableFunction = function myGreetings() {
		console.log("Updated Hello")
	}

	variableFunction();

// Function Scoping

	// Scope is the accessibility(visibility) of variables
	/*
		a. local/block scope
		b. global scope
		c. function scope
	*/

	{
		let localVar = "Armando Perez";
		console.log(localVar);
	}

	let globalVar = "Mr. Worldwide";

	console.log(globalVar);

// Function Scope

	function showNames() {
		// Function scoped variables:

		const functionConst = "John";
		let functionLet = "Jane";

		console.log (functionConst);
		console.log (functionLet);
	}

showNames();

// alert() and prompt()

	// alert() - allows us to show a small window at the top of our browser page to show information to our users.

	// alert("Hello, User!");

	// function showSampleAlert() {
	// 	alert("Hello User!");
	// }

	// showSampleAlert();

	// ======================================================

	// Prompt() - Allows us to show a small window at the top of the browser to gather user input.

	let samplePrompt = prompt("Enter your Name: ");

	console.log("Hello, " + samplePrompt);

	function printWelcomeMessages() {
		let firstName = prompt("Enter Your First Name");
		let lastName = prompt("Enter Your Last Name");

		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("Welcome to Mobile Legends");
	}
	
	printWelcomeMessages();